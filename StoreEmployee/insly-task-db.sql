
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `insly-task`
--

-- --------------------------------------------------------

--
-- Table structure for table `employeeContacts`
--

CREATE TABLE `employeeContacts` (
  `contactId` int(11) NOT NULL,
  `employeeId` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT 0 COMMENT 'format(unixtime)',
  `updated_at` int(11) NOT NULL DEFAULT 0 COMMENT 'format(unixtime)',
  `created_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employeeContacts`
--

INSERT INTO `employeeContacts` (`contactId`, `employeeId`, `email`, `phone`, `address`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, 'daniyar.islamgaliyev@gmail.com', '87474324644', 'Almaty city, Gagarin str. 177', 1550275200, 1550275200, 1710);

-- --------------------------------------------------------

--
-- Table structure for table `employeeInfoEng`
--

CREATE TABLE `employeeInfoEng` (
  `infoId_eng` int(11) NOT NULL,
  `employeeId_eng` int(11) NOT NULL,
  `intro_eng` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `workExp_eng` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `education_eng` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employeeInfoEng`
--

INSERT INTO `employeeInfoEng` (`infoId_eng`, `employeeId_eng`, `intro_eng`, `workExp_eng`, `education_eng`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but ', 'Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but ', 1550275200, 1550275200, 1710);



CREATE TABLE `employeeInfoFra` (
  `infoId_fra` int(11) NOT NULL,
  `employeeId_fra` int(11) NOT NULL,
  `intro_fra` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `workExp_fra` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `education_fra` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employeeInfoFra`
--

INSERT INTO `employeeInfoFra` (`infoId_fra`, `employeeId_fra`, `intro_fra`, `workExp_fra`, `education_fra`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, '\r\nEn français\r\nMaître Corbeau, sur un arbre perché,\r\nTenait en son bec un fromage.\r\nMaître Renard, par l’odeur alléché,\r\nLui tint à peu près ce langage :', '\r\nEn français\r\nMaître Corbeau, sur un arbre perché,\r\nTenait en son bec un fromage.\r\nMaître Renard, par l’odeur alléché,\r\nLui tint à peu près ce langage :', '\r\nEn français\r\nMaître Corbeau, sur un arbre perché,\r\nTenait en son bec un fromage.\r\nMaître Renard, par l’odeur alléché,\r\nLui tint à peu près ce langage :', 1550275200, 1550275200, 1710);

-- --------------------------------------------------------

--
-- Table structure for table `employeeInfoSpa`
--

CREATE TABLE `employeeInfoSpa` (
  `infoId_spa` int(11) NOT NULL,
  `employeeId_spa` int(11) NOT NULL,
  `intro_spa` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `workExp_spa` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `education_spa` text COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL DEFAULT 0,
  `updated_at` int(11) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employeeInfoSpa`
--

INSERT INTO `employeeInfoSpa` (`infoId_spa`, `employeeId_spa`, `intro_spa`, `workExp_spa`, `education_spa`, `created_at`, `updated_at`, `created_by`) VALUES
(1, 1, 'Rogue wireless hardware is easy to introduce. Wireless access points are relatively\r\ninexpensive and easily deployed. A well-intentioned team of consultants working in a\r\nconference room might install a wireless access point in order to share a single wire port\r\nin the room. ', 'Rogue wireless hardware is easy to introduce. Wireless access points are relatively\r\ninexpensive and easily deployed. A well-intentioned team of consultants working in a\r\nconference room might install a wireless access point in order to share a single wire port\r\nin the room. ', 'Rogue wireless hardware is easy to introduce. Wireless access points are relatively\r\ninexpensive and easily deployed. A well-intentioned team of consultants working in a\r\nconference room might install a wireless access point in order to share a single wire port\r\nin the room. ', 1550275200, 1550275200, 1710);

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `employeeId` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bday` int(11) NOT NULL DEFAULT 0 COMMENT 'format(unixtime)',
  `ssn` int(11) NOT NULL DEFAULT 0,
  `created_at` int(11) NOT NULL DEFAULT 0 COMMENT 'format(unixtime)',
  `updated_at` int(11) NOT NULL DEFAULT 0 COMMENT 'format(unixtime)',
  `created_by` int(11) NOT NULL DEFAULT 0 COMMENT 'id of user who created the employee',
  `isActive` int(1) NOT NULL COMMENT '1 - active, 0 - not'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`employeeId`, `name`, `bday`, `ssn`, `created_at`, `updated_at`, `created_by`, `isActive`) VALUES
(1, 'Daniyar', 870652800, 77777777, 1550275200, 1550275200, 1710, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employeeContacts`
--
ALTER TABLE `employeeContacts`
  ADD PRIMARY KEY (`contactId`),
  ADD KEY `employeeId` (`employeeId`);

--
-- Indexes for table `employeeInfoEng`
--
ALTER TABLE `employeeInfoEng`
  ADD PRIMARY KEY (`infoId_eng`),
  ADD KEY `employeeId_eng` (`employeeId_eng`);

--
-- Indexes for table `employeeInfoFra`
--
ALTER TABLE `employeeInfoFra`
  ADD PRIMARY KEY (`infoId_fra`),
  ADD KEY `employeeId_fra` (`employeeId_fra`);

--
-- Indexes for table `employeeInfoSpa`
--
ALTER TABLE `employeeInfoSpa`
  ADD PRIMARY KEY (`infoId_spa`),
  ADD KEY `employeeId_spa` (`employeeId_spa`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`employeeId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employeeContacts`
--
ALTER TABLE `employeeContacts`
  MODIFY `contactId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employeeInfoEng`
--
ALTER TABLE `employeeInfoEng`
  MODIFY `infoId_eng` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employeeInfoFra`
--
ALTER TABLE `employeeInfoFra`
  MODIFY `infoId_fra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employeeInfoSpa`
--
ALTER TABLE `employeeInfoSpa`
  MODIFY `infoId_spa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `employeeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `employeeContacts`
--
ALTER TABLE `employeeContacts`
  ADD CONSTRAINT `employeeContacts_ibfk_1` FOREIGN KEY (`employeeId`) REFERENCES `employees` (`employeeId`);

--
-- Constraints for table `employeeInfoEng`
--
ALTER TABLE `employeeInfoEng`
  ADD CONSTRAINT `employeeInfoEng_ibfk_1` FOREIGN KEY (`employeeId_eng`) REFERENCES `employees` (`employeeId`);

--
-- Constraints for table `employeeInfoFra`
--
ALTER TABLE `employeeInfoFra`
  ADD CONSTRAINT `employeeInfoFra_ibfk_1` FOREIGN KEY (`employeeId_fra`) REFERENCES `employees` (`employeeId`);

--
-- Constraints for table `employeeInfoSpa`
--
ALTER TABLE `employeeInfoSpa`
  ADD CONSTRAINT `employeeInfoSpa_ibfk_1` FOREIGN KEY (`employeeId_spa`) REFERENCES `employees` (`employeeId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
