
<?php if (!empty($policy)) { ?>
    <table>
        <tr>
            <th></th>
            <th>Policy</th>
            <?php
                if (!empty($instalments)) {
                    for ($i = 0; $i < count($instalments); $i++) {
                            echo '<th>' . ($i+1). 'installment ' . '</th>';
                    }
                }  
            ?>
        </tr>
        <tr>
            <td>Value:</td>
            <td><?php echo $policy['price'] ?></td>
        </tr>
        <tr>
            <td>Base price:</td>
            <td>
                <?php echo $policy['basePrice'] ?>
            </td>
            <?php
            if(!empty($instalments)) {
                for ($i = 0; $i < count($instalments); $i++) {
                    echo '<td>' . $instalments[$i]['basePrice'] . '</td>';
                }
            }
            ?>
        </tr>
        <tr>
            <td>Commission:</td>
            <td>
                <?php echo $policy['commission'] ?>
            </td>
            <?php
            if(!empty($instalments)) {
                for ($i = 0; $i < count($instalments); $i++) {
                    echo '<td>' . $instalments[$i]['commission'] . '</td>';
                }
            }
            ?>
        </tr>
        <tr>
            <td>Total tax:</td>
            <td>
                <?php echo $policy['totalTax'] ?>
            </td>
            <?php
            if(!empty($instalments)) {
                for ($i = 0; $i < count($instalments); $i++) {
                    echo '<td>' . $instalments[$i]['totalTax'] . '</td>';
                }
            }
            ?>
        </tr>
        <tr>
            <td>Total cost:</td>
            <td>
                <?php echo $policy['totalCost'] ?>
            </td>
            <?php
            if(!empty($instalments)) {
                for ($i = 0; $i < count($instalments); $i++) {
                    echo '<td>' . $instalments[$i]['totalCost'] . '</td>';
                }
            }
            ?>
        </tr>
    </table>
<?php } ?>