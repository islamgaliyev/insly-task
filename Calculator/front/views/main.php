<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="/<?=DIR?>/front/assets/css/style.css">
    </head>
    <body>
        <h3>Car insurance calculator</h3>
        <div>
            <form id="form" action="?r=calculator/calculate" method="post" onsubmit="return validateForm()">

                <label for="price">Value of the car(100 - 100 000 EUR)</label>
                <input type="text" id="price" name="price" placeholder="Estimated value of the car ...">
                

                <label for="tax">Tax percentage (0 - 100%)</label>
                <input type="text" id="tax" name="tax" placeholder="Tax percentage (0 - 100%) ...">

                <label for="instalment">Number of instalments(1-12)</label>
                <input type="text" id="instalment" name="instalment" placeholder="Count of payments in which client wants to pay for the policy ...">
            
                <input type="submit" value="Submit">
            </form>
        </div>
    </body>
    <?=Template::view('table.php')->with(['instalments' => $instalments, 'policy' => $policy])->partialRender(['test']) ?>
<script src="/<?=DIR?>/front/assets/js/index.js"></script>
</html>

