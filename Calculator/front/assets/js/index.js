validateForm = () => {

    let priceInput = document.forms["form"]["price"].value;
    let taxInput = document.forms["form"]["tax"].value;
    let instalmentInput = document.forms["form"]["instalment"].value;

    if(!isNumber(priceInput) || price >= 100000 || price <= 100 || priceInput == "") {
        alert('Invalid value of price!');
        return false;
    } else if(!isNumber(taxInput) || tax >= 100 || tax <= 0 || taxInput == "") {
        alert('Invalid value of tax!');
        return false;
    } else if(!isNumber(instalmentInput) || instalment > 12 || tax < 1 || instalmentInput == "") {
        alert('Invalid value of instalment!');
        return false;
    }

    return true;
}

isNumber = (val) => {
    let pattern = /^-?(\d+\.?\d*)$|(\d*\.?\d+)$/;
    return pattern.test(val.toString());
}