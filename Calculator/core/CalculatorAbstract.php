<?php

abstract class CalculatorAbstract
{
    protected $day;
    protected $hour;

    public function __construct()
    {
        $this->hour = date('H');
        $this->day = date('w');
    }

    protected function getBasePriceValue(float $value): float
    {
        $isDefaultCommission = $this->hour < 15 && $this->hour > 20 && $this->d != 5;
        if ($isDefaultCommission) {
            return $value * (Constant::DEFAULT_PRICE_OF_POLICY / 100);
        }
        return $value * (Constant::HIGH_PRICE_OF_POLICY / 100);
    }

    protected function calculateTax(float $basePrice, float $tax): float
    {
        return $basePrice * ($tax / 100);
    }

    protected function calculateCommission(float $basePrice): float
    {
        return $basePrice * (Constant::COMMISSION / 100);
    }
}