<?php

function dd($dd)
{
    echo '<pre>';
    print_r($dd);
    echo '</pre>';
    die();
}

function core($class)
{
    $file = DIR_SYSTEM . str_replace('\\', '/', strtolower($class)) . '.php';
    if (is_file($file)) {
        include_once($file);
        return true;
    } else {
        return false;
    }
}

spl_autoload_register('core');
spl_autoload_extensions('.php');

function start($start, $variable = [])
{
    header("Content-type: text/html; charset=utf-8");

    $request = new Request();
    $route = new Route($request);
    Template::view($start . '.php')->render();
}