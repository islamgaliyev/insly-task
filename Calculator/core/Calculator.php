<?php

class Calculator extends CalculatorAbstract implements CalculatorInterface
{
  public function __construct()
  {
  }

  function calculate(Request $r)
  {

    $price = $r->post['price'];
    $tax = $r->post['tax'];
    $instalment = $r->post['instalment'];

    $basePrice   = $this->getBasePriceValue((float)$price);
    $totalTax    = $this->calculateTax((float)$basePrice, (float)$tax);
    $commission  = $this->calculateCommission((float)$basePrice);

    $totalCost   = $basePrice + $totalTax + $commission;


    $policy = [
      'price'      => $price,
      'basePrice'  => $basePrice,
      'totalTax'   => $totalTax,
      'commission' => $commission,
      'totalCost'  => $totalCost
    ];

    $instalments = array();

    for ($i = 0; $i < $instalment; $i++) {

        array_push($instalments, [
          'basePrice'  => $basePrice / $instalment,
          'totalTax'   => $totalTax / $instalment,
          'commission' => $commission / $instalment,
          'totalCost'  => ($basePrice / $instalment) + ($totalTax / $instalment) + ($commission / $instalment),
        ]);
    }

    $formattedPolicy = Service::formatValueOfArray($policy);
    $formattedInstalment = Service::formatValueOfArray($instalments);

    Template::view('main.php')->with(['instalments' => $formattedInstalment, 'policy' => $formattedPolicy])->render();
  }
  
}