<?php

class Service
{
    public static function formatValueOfArray($array): array
    {
        array_walk_recursive($array,  function(&$item) {
            $item = number_format((float)$item, 2, '.', '');
        });
        return $array;
    }
}