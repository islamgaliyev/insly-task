<?php

class Constant
{
    const HIGH_PRICE_OF_POLICY = 13;
    const DEFAULT_PRICE_OF_POLICY = 11;
    const COMMISSION = 17;
}