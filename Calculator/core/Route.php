<?php

class Route
{
    private $action;
    private $controller = null;
 
    public function __construct(Request $r)
    {

        if(isset($r->get["r"])) {
            $this->action = explode('/', $r->get["r"]);
            if (!empty($this->action)) {
                $this->controller = $this->action[0];
                $this->method = $this->action[1];
                $this->route($r);
            }
        }
    }
    public function route($request)
    {   
        $controller = ucfirst($this->controller);
        $method = $this->method;
        if (method_exists($this->controller, $this->method) && is_callable(array($this->controller, $this->method))) { 
            $object = new $controller;
            call_user_func_array(array($object, $method), [$request]);
        }
    }
}