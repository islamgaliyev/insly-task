<?php

class Template
{
    public static $template;
    public static $view = array();
    public static $partialView = array();
    public static $variables = array();
    protected static $instance;

    
    protected function __construct()
    {
    }

    protected static function getSelf()
    {
        if (static::$instance === null) {
            static::$instance = new Template;
        }
        return static::$instance;
    }

    public static function partialRender($content = array())
    {
        global $partialView;

        if (!is_null(self::$variables)) {
            extract(self::$variables);
        }
        
        ob_start();
        $partialView = (array)self::$partialView;

        require_once DIR_TEMPLATE . self::$template;
        
        $htmlfile = ob_get_clean();
        echo $htmlfile;
    }

    public static function render()
    {
        global $view;

        if (!is_null(self::$variables)) {
            extract(self::$variables);
        }

        ob_start();
        $view = (array)self::$view;

        require_once DIR_TEMPLATE . self::$template;
        $htmlfile = ob_get_clean();
        
        echo $htmlfile;

        return $view;
    }

    public static function view($filename)
    {
        self::$template = $filename;
        return static::getSelf();
    }

    public static function with($data)
    {
        
        foreach($data as $key => $value) {
            self::$variables[$key] = $value;
        }

        return static::getSelf();
    }
}