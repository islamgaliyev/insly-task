<?php

interface CalculatorInterface
{
    public function calculate(Request $r);
}